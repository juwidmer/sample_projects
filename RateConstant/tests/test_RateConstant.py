#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 16:29:20 2021

@author: jwidmer
"""
import numpy as np
import unittest
from .. import RateConstantMLE as rc

class Test_RateConstant(unittest.TestCase):
    a = np.array([1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1])
    a_inter = np.array([0.5, 1, 0.5, 0.5, 0, 0, 0.5, 0, 0.5, 1, 1, 0])
    empty1 = np.array([0, 0, 0, 0])

    def test_consecutive_runs(self):
        a = self.a
        empty1 = self.empty1
        # Count 1s
        ind, stretches = rc.consecutive_stretches_starting_from_1(a)
        assert (ind == [[0, 2], [5, 9]]).all()
        # Count 0s
        ind, stretches = rc.consecutive_stretches_starting_from_1(1 - a)
        assert (ind == [[2, 5], [9, 11]]).all()
        # Return empty array if there are no transitions
        assert rc.consecutive_stretches_starting_from_1(empty1)[0].size == 0
        assert rc.consecutive_stretches_starting_from_1(1 - empty1)[0].size == 0



    def test_consecutive_two_cutoff(self):
        a_inter = self.a_inter
        # Count 1s
        ind, stretches = rc.consecutive_stretches_starting_from_1(a_inter)
        assert (ind == [[1, 4], [7, 9]]).all()
        # Count 0s
        ind, stretches = rc.consecutive_stretches_starting_from_1(1 - a_inter)
        assert (ind == [[4, 7]]).all()



    def test_consecutive_for_k_frames(self):
        a_inter = self.a_inter
        # For k=0, it should be the same indices returned
        assert np.allclose(rc.consecutive_for_k_frames(a_inter, k=0)[0],
                           rc.consecutive_stretches_starting_from_1(a_inter)[0])
        # subset of all 1-stretches is returned
        assert np.allclose(rc.consecutive_for_k_frames(a_inter, k=1)[0],
                           [[1, 4]])
        # If k exceeds length of all 0 stretches, return empty array
        assert (rc.consecutive_for_k_frames(a_inter, k=100)[0]).size == 0



    def test_exp_fit(self):
        binding_times = np.random.exponential(5, size=100000)
        x_array, F_y_inv, params, param_cov = rc.fit_fn_to_binding_times(binding_times,
                                                                         rc.exponential,
                                                                         p0=None)
        np.testing.assert_array_almost_equal(params, [1., 5.], decimal=1)
        # MLE of \tau is equal to the sample mean of binding times
        np.testing.assert_array_almost_equal(np.mean(binding_times), 5., decimal=1)



    def test_doubleexp_fit(self):
        binding_times1 = np.random.exponential(5, size=100000)
        binding_times2 = np.random.exponential(50, size=100000)
        binding_times = np.concatenate([binding_times1, binding_times2])
        x_array, F_y_inv, params, param_cov = rc.fit_fn_to_binding_times(binding_times,
                                                                         rc.double_exponential,
                                                                         p0=None)
        np.testing.assert_array_almost_equal(params, [0.5, 50., 5.], decimal=0)

       

    def test_calculate_conc(self):
        n_ligand = 6.022e23
        volume_in_nm3 = 1e24
        conc_ligand = rc.calculate_conc(volume_in_nm3, n_ligand)
        np.testing.assert_array_almost_equal(conc_ligand, 1)



    def test_first_order(self):
        order = 1
        dt = 1 # trajectory time step in seconds 
        conc_ligand = 0.5 # mol/L; inconsequential for first order
        estimated_lt_param = 1 # estimated from k * exp(-t/\tau)
        k_on = rc.calculate_k_on(conc_ligand, avg_lifetime=estimated_lt_param,
                                 timescale=dt, order=order)
        assert k_on == 1
        # Quadrupling binding times should quarter the rate constant
        estimated_lt_param = 4
        k_on = rc.calculate_k_on(conc_ligand, avg_lifetime=estimated_lt_param,
                                 timescale=dt, order=order)
        assert k_on == 0.25

