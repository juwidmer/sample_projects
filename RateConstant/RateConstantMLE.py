#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 14:48:23 2021

@author: jwidmer
"""
import numpy as np
from typing import Callable, Tuple
from scipy.optimize import curve_fit

'''
Module to infer rate constants directly from trajectories.

Suggested workflow:
    unbound = partition_array(data, cutoff_high=unbound_cutoff,
                                 cutoff_low=bound_cutoff)
    _, binding_times = consecutive_for_k_frames(unbound, k=1)
    x_array, F_y_norm, params, _ = fit_fn_to_binding_times(binding_times,
                                                           exponential)
    conc_ligand = calculate_conc(volume_in_nm3, n_ligand)
    k_on = calculate_k_on(conc_ligand=conc_ligand, order=1,
                          avg_lifetime=params[0], timescale=10e-9)
'''
def consecutive_integers(data: np.ndarray, stepsize: int = 1) -> np.ndarray:
    """Returns indices in array where integers are consecutive."""
    return np.split(data, np.where(np.diff(data) != stepsize)[0]+1)


def partition_array(data: np.ndarray, cutoff_low: float,
                    cutoff_high: float) -> np.ndarray:
    """0 where data < cutoff_low (bound)
    0.5 where cutoff_low < data < cutoff_high
    1 where data > cutoff_high (fully unbound)"""
    close = np.where(data < cutoff_low)
    far = np.where(data > cutoff_high)
    unbound = np.ones_like(data) - 0.5
    unbound[close] = 0
    unbound[far] = 1
    return unbound


# https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Kinetics/02%3A_Reaction_Rates/2.03%3A_First-Order_Reactions
def consecutive_stretches_starting_from_1(unbound: np.ndarray, stepsize=1):
    """Returns indices of stretches of consecutive elements > 0,
    starting at the first 1 and stopping at the first 0 encountered.
    In the context of binding, it gives the stretches the ligand takes to bind,
    starting from 1 (= fully unbound)"""
    unbound = np.array(unbound)
    # Frames where RNA is fully unbound
    equal_1 = np.where(unbound == 1)[0]
    # Frames where RNA is not bound
    fully_unbound = np.nonzero(unbound)[0]
    # Stretches where RNA is consecutively unbound
    consecutive_unbound = consecutive_integers(fully_unbound, stepsize)
    # The reduced stretches starting at the first 1 (fully unbound)
    stretch_indices = []
    for stretch in consecutive_unbound:
        # Find the first 1 in the consecutive stretches of non-zeros
        one_in_stretch = np.isin(stretch, equal_1)
        if not one_in_stretch.any():
            continue
        start = np.argmax(one_in_stretch)
        stretch_index = [stretch[start], stretch[-1] + 1]
        # Don't count if it never leads to binding
        if stretch_index[1] != unbound.shape[0]:
            stretch_indices.append(stretch_index)
    stretch_lens_1 = np.diff(stretch_indices)

    return np.array(stretch_indices).reshape(-1, 2), stretch_lens_1.reshape(-1)


def consecutive_for_k_frames(a: np.ndarray, k: int, stepsize=1) -> np.ndarray:
    """Find stretches of != 0 starting from 1 where any subsequent stretch of 0 is > k.
    Return array with length of 0-stretches."""

    one_ranges, stretch_lens_1 = consecutive_stretches_starting_from_1(a, stepsize)

    # Create an array that is 1 where a is equal to 0,
    # and pad each end with an extra 0.
    isvalue = np.concatenate(([0], np.equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(isvalue))
    # Runs start and end where absdiff is 1.
    zero_ranges = np.where(absdiff != 0)[0].reshape(-1, 2)
    
    indices_greater_k = []
    stretch_lens_1_k = []
    for i, stretch in enumerate(one_ranges):
        start = stretch[1]
        try:
            stop = one_ranges[i + 1][0]
        except IndexError:
            stop = a.shape[0]
        between = [i for i in zero_ranges if (i >= start).all() & (i <= stop).all()]
        if (np.diff(between) > k).any():
            indices_greater_k.append(stretch)
            stretch_lens_1_k.append(i)

    return np.array(indices_greater_k), stretch_lens_1[stretch_lens_1_k].reshape(-1)


def exponential(x: np.ndarray, tau: float) -> np.ndarray:
    return np.exp(-x / tau)


def double_exponential(x, fact, tau1, tau2):
    return fact * np.exp(-x / tau1) + (1-fact) * np.exp(-x / tau2)


def gen_label(params):
    if len(params) == 3:
        return (r"y = %.2f $\exp$[-$\frac{t}{%.1f}$] + %.2f $\exp$[-$\frac{t}{%.1f}$]"
                % (params[0], params[1], 1-params[0], params[2]))
    elif len(params) == 1:
        return r"y = $\exp$[-$\frac{t}{%.1f}$]" % params[0]


def fit_fn_to_binding_times(binding_times: np.ndarray, function: Callable,
                            **kwargs) -> Tuple:
    """Fits binding times to its one minus empirical CDF using a function using 
    least squares. If binding times come from an exponential distribution,
    1 - CDF has the form of exp(-\lambda t), which is equal to the concentration-
    normalized first order integrated rate law. Note that the MLE of \lambda is
    equal to the sample mean of binding times.
    NOTE: It can be critical to supply reasonable parameter bounds via kwargs."""
    x_array = np.sort(binding_times)
    # Does not break ties; not sure if problematic
    F_y = np.arange(x_array.shape[0])/x_array.shape[0]
    F_y_inv = 1 - F_y

    popt_exponential, pcov_exponential = curve_fit(f=function, xdata=x_array,
                                                   ydata=F_y_inv, **kwargs)
    return x_array, F_y_inv, popt_exponential, pcov_exponential


def calculate_conc(volume_in_nm3: float, n_ligand: int = 1) -> float:
    """Calculate molar concentration in simulation box. Volume of box can be
    found by calling gmx editconf on the .gro file."""
    mol = 6.022e23
    volume_in_liters = volume_in_nm3 * 1e-24
    conc_ligand = n_ligand / mol / volume_in_liters
    return conc_ligand


def calculate_k_on(conc_ligand: float, avg_lifetime: float, timescale: float,
                   order: int = 1) -> float:
    """Calculate k_on from average binding times and the ligand concentration
    with the specified order kinetics. Returns k_on. Timescale is the factor
    to convert the unit of the avg_lifetime to seconds"""
    # avg_lifetime is \tau in np.exp(-x / \tau)
    k_on = 1 / avg_lifetime / conc_ligand**(order-1) / timescale
    return k_on

