#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 10:08:00 2024

@author: jwidmer
"""
import numpy as np
from typing import Dict, List, Tuple

"""Calculate rate constants and dissociation constants according to transition
path theory"""

def calculate_total_current(clusters: List[int], bound_clusters: List[int],
                            unbound_clusters: List[int], Pi: Dict[int, float],
                            Tij: Dict[Tuple[(int, int)], float], Qj: Dict[int, float],
                            source: str):
    """
    clusters: List of unique cluster indices
    
    bound_clusters, unbound_clusters: Subset of clusters indicating boundaries
    
    Pi: Dict mapping to cluster indices their stationary weight
    
    Tij: Dict mapping to tuple ofcluster indices i and j the transition
        probability i->j
    
    Qj: Dict mapping to each cluster index j the plus-committor
    
    source, str:
        source == 'unbound' calculates k_bind,
        source == 'bound' calculates k_unbind 
    """
    
    if source == 'unbound':
        source_set = unbound_clusters
    elif source == 'bound':
        source_set = bound_clusters
    
    ## Calculate total current F
    total_current_F = 0
    for cl_i in source_set: # Iterate over cluster i in source set
        for cl_j in clusters: # Iterate over sink cluster j NOT in source set
            if cl_j in source_set:
                continue
            pi = Pi[cl_i]
            # Some clusters are not in the connected component. The contribution
            # of those clusters is set to 0
            tij = Tij.get((cl_i, cl_j), 0)
            if source == 'unbound':
                qj = Qj.get(cl_j, 0)
            # If the source and sink are swapped, q+ and q- should just be 1-q
            # since 0 and 1 are swapped but the transition matrix stays the same
            elif source == 'bound':
                qj = 1 - Qj.get(cl_j, 0)

            total_current_F += pi * qj * tij
    return total_current_F


def calculate_rate_constant(total_current_F: float, Qj_m: Dict[int, float],
                            Pi: Dict[int, float], TAU_LAG: float, source: str):
    """Takes as input the total net current F and divides it by the minus-committor
    q- weighted by the stationary probability to return the rate constant.

    total_current_F: Float returned by calculate_total_current

    Pi: Dict mapping to cluster indices their stationary weight
    
    Qj_m: Dict mapping to each cluster index j the minus-committor
    
    TAU_LAG: The lag time of the Markov chain in the desired units of time
    
    source, str:
        source == 'unbound' calculates k_bind,
        source == 'bound' calculates k_unbind """


    denom = 0
    for cl_i in Qj_m:
        if source == 'unbound':
            qj_m = Qj_m[cl_i]
        elif source == 'bound':
            qj_m = 1 - Qj_m[cl_i]
        denom += Pi[cl_i] * qj_m
    k = total_current_F / (TAU_LAG * denom)
    return k


def get_free_fraction(Pi, Qj, thresh: float):
    """Splits trajectory into complex and free ligand/receptor according to
    the specified committor threshold. Returns the fraction of free ligand
    and receptor according to the stationary distribution."""

    free_frac = 0
    for cl_i in Qj:
        if Qj[cl_i] <= thresh:
            free_frac += Pi[cl_i]

    return free_frac



def calc_KD(conc: float, free_frac: float, nu_bind: float, nu_unbind: float):
    """Calculate rate constants and dissociation constants according to transition
    path theory. For comparison, return the KD based on a two-state model.
    Mass action is used to calculate species concentrations. Mass action kinetics
    with backflux set to zero are used to solve analytically for rate constants."""
    # Concentration of free RNA and free protein in solution: [L]*[R]
    conc_free = (conc * free_frac)**2
    # Concentration of the complex in solution: [LR]
    conc_complex = (conc * (1-free_frac))
    # Kd based on stationary probabilities only, no kinetic info
    Kd_twostate = conc_free / conc_complex

    # nu_bind = [L] * [R] * k_bind -> k_bind = nu_bind / ([L] * [R])
    k_bind = nu_bind / conc_free
    # nu_unbind = [LR] * k_unbind -> k_unbind = nu_unbind / [LR]
    k_unbind = nu_unbind / conc_complex
    # Kd considering rate constant
    Kd_TPT = (k_unbind/k_bind)
    
    Kd_twostate, Kd_TPT
    return k_bind, k_unbind, Kd_TPT, Kd_twostate


