#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 11:19:05 2024

@author: jwidmer
"""
import collections
import numpy as np
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform

"""Calculate similarity of pathways in a Markov Chain, i.e., sequences of 
cluster indices. String similarity is computed based on Gestalt pattern matching.
Similarity can be calculated either agnostic of the ordering, focussing solely
on the set of visited states.

Alternatively, the ordering can be taken into account.
Because pathways may have different length, they need to be aligned first.
This is achieved by the Needleman-Wunsch-algorithm, which comes from genetics.
Importantly, it permits inclusion of prior (or orthogonal) knowledge of two states being
related to impose less stringent penalization of mismatches.
"""


def Needleman_Wunsch(x, y, match=1, mismatch: dict = 1, gap=1):
    """Implementation of Needleman-Wunsch algorithm for optimal alignment of two
    strings given a cost for mismatches and gaps and a reward for matches."""
    nx = len(x)
    ny = len(y)
    
    # If the mismatch is a constant number, make that value the default for dict.get
    if type(mismatch) != dict:
        default = mismatch
        mismatch = dict()
    else:
        default = 1
    # Optimal score at each possible pair of characters.
    F = np.zeros((nx + 1, ny + 1))
    F[:,0] = np.linspace(0, -nx * gap, nx + 1)
    F[0,:] = np.linspace(0, -ny * gap, ny + 1)
    # Pointers to trace through an optimal aligment.
    P = np.zeros((nx + 1, ny + 1))
    P[:,0] = 3
    P[0,:] = 4
    # Temporary scores.
    t = np.zeros(3)
    for i in range(nx):
        for j in range(ny):
            # First, consider that there is no indel (diagonal move in F)
            if x[i] == y[j]: # If states match, increment reward
                t[0] = F[i, j] + match
            else: # otherwise penalize
            # Get the mismatch penalty from the dict, with the default being 1
                mismatch_penalty = min(mismatch.get((x[i], y[j]), default),
                                       mismatch.get((y[j], x[i]), default))
                t[0] = F[i, j] - mismatch_penalty
            # Second, consider there is a deletion...
            # ... in x (horizontal move in F)
            t[1] = F[i, j+1] - gap
            # ... in y (vertical move in F)
            t[2] = F[i+1, j] - gap
            # Record maximum score
            tmax = np.max(t)
            F[i+1,j+1] = tmax
            # Record best move to trace out the optimal alignment below
            if t[0] == tmax:
                P[i+1,j+1] += 2
            if t[1] == tmax:
                P[i+1,j+1] += 3
            if t[2] == tmax:
                P[i+1,j+1] += 4
    # Trace through an optimal alignment.
    i = nx
    j = ny
    rx = []
    ry = []
    while i > 0 or j > 0:
        if P[i,j] in [2, 5, 6, 9]:
            rx.append(x[i-1])
            ry.append(y[j-1])
            i -= 1
            j -= 1
        elif P[i,j] in [3, 5, 7, 9]:
            rx.append(x[i-1])
            ry.append('-')
            i -= 1
        elif P[i,j] in [4, 6, 7, 9]:
            rx.append('-')
            ry.append(y[j-1])
            j -= 1
    # Reverse the strings.
    rx = rx[::-1]
    ry = ry[::-1]
    return (rx, ry)


def string_distance(pw1: str, pw2: str, ignore_ordering: bool, **scores) -> float:
    """Return an upper bound on ratio(). 
    ignore_ordering, bool:
        If true, ignores the ordering of the visited states. If false,
        use Needleman-Wunsch algorithm to create an alignment of the cluster
        sequences. This allows introduction of gaps, which makes counting
        only exactly matched states more meaningful.
    **scores, dict:
        Keywords with penalty and match scores passed to Needleman-Wunsch
        algorithm for aligning the two pathways.
    https://en.wikipedia.org/wiki/Gestalt_pattern_matching."""
    pw1 = pw1[0].split('.')
    pw2 = pw2[0].split('.')
    
    # Ignore ordering: Just check same states are visited along path
    if ignore_ordering == True:
        l1, l2 = len(pw1), len(pw2)
        intersect = collections.Counter(pw1) & collections.Counter(pw2)
        # Get total number of matched states along path, subtract alignment gaps
        matches = sum(intersect.values())
    # Respect ordering: Check where the aligned pathways are equivalent
    elif ignore_ordering == False:
        # Run alignment alogrithm to account for different length of paths
        s1, s2 = Needleman_Wunsch(pw1, pw2, **scores)
        # Measure length of actual pathway without alignment gaps
        l1 = len(s1) - s1.count('-')
        l2 = len(s2) - s2.count('-')
        # Number of exactly matched state in alignment
        exact_match = np.array(s1) == np.array(s2)
        matches = exact_match.sum()
        # If a dictionary is passed that gives the distance in the interval
        # (0, 1) between clusters i and j, calculate its contribution to matches
        if 'mismatch' in scores:
            if type(scores['mismatch']) == dict:
                # Default: Don't add any contribution to matches, for example
                # if one of the characters in the string is '-', the deletion
                matches += sum([scores['mismatch'].get((s1[i], s2[i]), 0)
                                for i in np.where(~exact_match)[0]])
                # Add the reverse combination, so that only i, j needs to be in there
                matches += sum([scores['mismatch'].get((s2[i], s1[i]), 0)
                                for i in np.where(~exact_match)[0]])

    length = l1 + l2
    match_similarity = (2.0 * matches / (length - abs(l1 - l2)/2))

    return 1 - match_similarity


def get_cluster_medioids(clusters, pws,  **kwargs):
    """Return medioids of each cluster.
    clusters:
        1D-array of length N containing cluster indices that indicating membership
        of each pathway.
    pws:
        2D-array of shape (N, 1) containing pathways.
    
    Returns list of pathways of length len(set(clusters))."""
    assert len(pws) == len(clusters)
    # Calculate centroids for each cluster
    medioids = dict()
    for i in np.unique(clusters):
        cluster_member_indices = np.where(clusters == i)
        # If singlet cluster, the medioid is trivial
        if len(cluster_member_indices) == 1:
            medioid_pw = pws[cluster_member_indices][0][0]
        else:
            # Medioid: Sum of distances to all the objects in the cluster is minimal
            # index of the pathway that is closest to any other pathway in the cluster
            # Might be a smarter way to get the within-cluster distances
            # without recalculating subsets
            Ds = squareform(pdist(pws[cluster_member_indices],
                                  metric=string_distance,
                                  **kwargs))
            medioid_id = np.argmin(Ds.sum(axis=0))
            medioid_pw = pws[cluster_member_indices[medioid_id]]

        medioids[i]  = medioid_pw
    return medioids

#%%
if __name__ == '__main__':
    TEST = True
    if TEST == True:
        ## Run some example pathways
        x = ['.'.join(['49', '67', '227', '101', '961', '296', '770', '1562', '738'])]
        # skip 101, 961; go through 5 instead of 770, 1562
        y = ['.'.join(['49', '67', '227', '296', '5', '738'])]
        # Same as y, except 227 -> 67 instead of 67 -> 227
        y_ = ['.'.join(['49', '227', '67', '296', '5', '738'])]
        # If prior knowledge dictates that two states 67 and 227 are closely
        # related, the mismatch is penalized less stringently
        mismatch = {('67', '227'): 0.2}
        
        print('x, y')
        print(string_distance(x, y, ignore_ordering=True))
        print(Needleman_Wunsch(x[0].split('.'), y[0].split('.')))
        print(string_distance(x, y, ignore_ordering=False))
        
        print('\n  x, y_')
        print(string_distance(x, y_, ignore_ordering=True))
        print(Needleman_Wunsch(x[0].split('.'), y_[0].split('.')))
        print(string_distance(x, y_, ignore_ordering=False))
        
        print('\n y, y_')
        print(string_distance(y, y_, ignore_ordering=True))
        print(Needleman_Wunsch(y[0].split('.'), y_[0].split('.')))
        print(string_distance(y, y_, ignore_ordering=False))
        # Penalize gap more so that inversions are not compensated this way
        print(string_distance(y, y_, ignore_ordering=False, gap=2))
        # Provide mismatch penalty alleviation (conformationally close clusters)
        print(string_distance(y, y_, ignore_ordering=False, gap=2, mismatch=mismatch))
    
        pdist([x, y, y_], metric=string_distance, ignore_ordering=True)
        pdist([x, y, y_], metric=string_distance, ignore_ordering=False)
        # Confirm custom scores are passed to Needleman Wunsch
        pdist([x, y, y_], metric=string_distance, ignore_ordering=False,
              mismatch=1, gap=2)
