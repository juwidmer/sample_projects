#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 17:23:20 2021

@author: julian
"""
import numpy as np

from msmtools.estimation import count_matrix, transition_matrix
from pyemma.msm import markov_model
from pyemma.util.metrics import vamp_score
from pyemma.util.linalg import submatrix

"""Calculate VAMP2-scores (from https://link.springer.com/article/10.1007/s00332-019-09567-y)
for Markov chains with custom modification (i.e., priors) of the count matrix."""

def custom_count(dtraj: np.ndarray, lag: int, symmetric: str,
                 mincount: int = 0) -> np.ndarray:
    C = count_matrix(dtraj, lag=lag, sparse_return=False)

    if symmetric == 'max':
        # where ij is zero, but ji not, set to ij
        i, j = np.where((C == 0) & (C.T != 0))
        C[i, j] = C.T[i, j]
    elif symmetric == 'pad':
        # set all ij and ji to the higher value of the two
        i, j = np.where((C==0) & (C.T != 0))
        C[i, j] = C.T[i, j]
        i, j = np.where((C != C.T))
        C[i, j] = np.maximum(C[i, j], C.T[i, j])
    elif symmetric == 'mean':
        # where ij is zero, but ji not, set to the mean value
        i, j = np.where((C == 0) & (C.T != 0))
        mean = (C[i, j] + C.T[i, j]) / 2
        C[i, j] = mean
        C[j, i] = mean
    elif symmetric == False:
        pass
    
    # Set elements < mincount to zero
    C[C < mincount] = 0
    
    return C


def estimate_MSM(dtraj: np.ndarray, lag: int, symmetric: str = False,
                 mincount: int = 0, **kwargs):
    
    C = custom_count(dtraj, lag, symmetric, mincount)
    T = transition_matrix(C, **kwargs)
    mm = markov_model(T)

    return C, T, mm


def visited_set(C: np.ndarray) -> np.ndarray:
    """Identifies non-zero rows in to count matrix to define the visited set."""
    return np.where(C.any(axis=1))[0]


def score(dtrajs_train, dtrajs_test, tmat_train, lag, score_method="VAMP2",
          score_k=None, symmetric: str = False, mincount: int = 0,
          is_tmat_active: bool = True) -> float:
    """ 
    Scores the MSM using the variational approach for Markov processes [1]_ [2]_
    dtrajs_train and tmat_train are used to estimate the model; dtrajs_test are
    used to calculate the score.
    Currently only implemented using dense matrices - will be slow for large state spaces.
    Parameters
    ----------
    dtrajs_train: list of arrays
        train data (discrete trajectories).
    dtrajs_test : list of arrays
        test data (discrete trajectories).
    tmat_train: ndarray((?, ?))
        transition matrix calculated on the train set.
        This should have the same indices as the trajectory (i.e. no
        relabeling!).
    lag: int
        MSM lag time.
    score_method : str, optional, default='VAMP2'
        Overwrite scoring method to be used if desired. If `None`, the estimators scoring
        method will be used.
        Available scores are based on the variational approach for Markov processes [1]_ [2]_ :
        *  'VAMP1'  Sum of singular values of the symmetrized transition matrix [2]_ .
                    If the MSM is reversible, this is equal to the sum of transition
                    matrix eigenvalues, also called Rayleigh quotient [1]_ [3]_ .
        *  'VAMP2'  Sum of squared singular values of the symmetrized transition matrix [2]_ .
                    If the MSM is reversible, this is equal to the kinetic variance [4]_ .
    score_k : int or None
        The maximum number of eigenvalues or singular values used in the
        score. If set to None, all available eigenvalues will be used.
    References
    ----------
    .. [1] Noe, F. and F. Nueske: A variational approach to modeling slow processes
        in stochastic dynamical systems. SIAM Multiscale Model. Simul. 11, 635-655 (2013).
    .. [2] Wu, H and F. Noe: Variational approach for learning Markov processes
        from time series data (in preparation)
    .. [3] McGibbon, R and V. S. Pande: Variational cross-validation of slow
        dynamical modes in molecular kinetics, J. Chem. Phys. 142, 124105 (2015)
    .. [4] Noe, F. and C. Clementi: Kinetic distance and kinetic maps from molecular
        dynamics simulation. J. Chem. Theory Comput. 11, 5002-5011 (2015)
    """
    lag = int(lag)
    # score_k = int(score_k)
    C0t_train_raw = custom_count(dtrajs_train, lag=lag, symmetric=symmetric,
                                 mincount=mincount)
    # Determining active set (for fully connected components it is the visited set)
    # Visited set is wherever the transition matrix's row is != 0 (see symmetric).
    vis_set = visited_set(C0t_train_raw)
    nstates = len(vis_set)

    C0t_train = submatrix(C0t_train_raw, sel=vis_set)

    if not is_tmat_active:
        K = submatrix(tmat_train, vis_set)  # reduce to active set
    else:
        K = tmat_train
    
    from scipy.sparse import issparse
    if issparse(K):  # can't deal with sparse right now.
        K = K.toarray()
    if issparse(C0t_train):  # can't deal with sparse right now.
        C0t_train = C0t_train.toarray()

    ## Calc timelagged covariance matrices on training data
    C00_train = np.diag(C0t_train.sum(axis=1))  # empirical cov
    Ctt_train = np.diag(C0t_train.sum(axis=0))  # empirical cov
    # test data
    C0t_test_raw = custom_count(dtrajs_test, lag=lag, mincount=mincount,
                                symmetric=symmetric)
    # map to present active set
    # States from test data that are also in training data
    # You can calculate the vamp_score only on the common subspace between the train and the test set.
    # The transition matrix of the training trajectory is defined on the active set only:
    # meaning if you have a trajectory [0,1,3,5] you will have a 4x4 count matrix,
    # although the full number of states is 6 (from 0 to 5).
    # Now when you have the following test trajectory: [0, 1, 2, 6, 17]
    # you will get a 18x18 count matrix (as the highest state has index 17,
    # so this is the full number of states). From this you will want to keep only the
    # states that are common to the train trajectory (0,1) and map them back to a 4x4 matrix.
    # This is achieved by the map_from to map_to indices. In my test I am assuming that active set == visited set,
    # otherwise you should consider only the states in the largest connected component.
    map_from = vis_set[np.where(vis_set < C0t_test_raw.shape[0])[0]]
    map_to = np.arange(len(map_from))
    C0t_test = np.zeros((nstates, nstates))
    C0t_test[np.ix_(map_to, map_to)] = C0t_test_raw[np.ix_(map_from, map_from)]
        
    C00_test = np.diag(C0t_test.sum(axis=1))
    Ctt_test = np.diag(C0t_test.sum(axis=0))

    # score
    return vamp_score(K, C00_train, C0t_train, Ctt_train, C00_test, C0t_test, Ctt_test,
                      k=score_k, score=score_method)


def score_cv(dtrajs, lag: int, symmetric: str, count_mode: str = "sliding", n=10,
             score_method='vamp2', score_k=None, mincount: int = 0,
             **transition_params) -> np.ndarray:
        """ Scores the MSM using the variational approach for Markov processes [1]_ [2]_ and crossvalidation [3]_ .

        Divides the data into training and test data, fits a MSM using the training
        data using the parameters of this estimator, and scores is using the test
        data.
        Currently only one way of splitting is implemented, where for each n,
        the data is randomly divided into two approximately equally large sets of
        discrete trajectory fragments with lengths of at least the lagtime.

        Currently only implemented using dense matrices - will be slow for large state spaces.

        Parameters
        ----------
        dtrajs : list of arrays
            Test data (discrete trajectories).
        n : number of samples
            Number of repetitions of the cross-validation. Use large n to get solid
            means of the score.
        score_method : str, optional, default='VAMP2'
            Overwrite scoring method to be used if desired. If `None`, the estimators scoring
            method will be used.
            Available scores are based on the variational approach for Markov processes [1]_ [2]_ :

            *  'VAMP1'  Sum of singular values of the symmetrized transition matrix [2]_ .
                        If the MSM is reversible, this is equal to the sum of transition
                        matrix eigenvalues, also called Rayleigh quotient [1]_ [3]_ .
            *  'VAMP2'  Sum of squared singular values of the symmetrized transition matrix [2]_ .
                        If the MSM is reversible, this is equal to the kinetic variance [4]_ .

        score_k : int or None
            The maximum number of eigenvalues or singular values used in the
            score. If set to None, all available eigenvalues will be used.

        References
        ----------
        .. [1] Noe, F. and F. Nueske: A variational approach to modeling slow processes
            in stochastic dynamical systems. SIAM Multiscale Model. Simul. 11, 635-655 (2013).
        .. [2] Wu, H and F. Noe: Variational approach for learning Markov processes
            from time series data (in preparation).
        .. [3] McGibbon, R and V. S. Pande: Variational cross-validation of slow
            dynamical modes in molecular kinetics, J. Chem. Phys. 142, 124105 (2015).
        .. [4] Noe, F. and C. Clementi: Kinetic distance and kinetic maps from molecular
            dynamics simulation. J. Chem. Theory Comput. 11, 5002-5011 (2015).

        """
        from pyemma.util.types import ensure_dtraj_list
        from pyemma.msm.estimators._dtraj_stats import cvsplit_dtrajs
        from pyemma.msm.estimators import MaximumLikelihoodMSM

        dtrajs = ensure_dtraj_list(dtrajs)  # ensure format

        if count_mode not in ('sliding', 'sample'):
            raise ValueError('score_cv currently only supports count modes "sliding" and "sample"')
        sliding = count_mode == 'sliding'
        scores = []
        # Empty class is used only to split trajectories in training & test set
        estimator = MaximumLikelihoodMSM(lag=lag)
        for i in range(n):

            dtrajs_split = estimator._blocksplit_dtrajs(dtrajs, sliding)
            dtrajs_train, dtrajs_test = cvsplit_dtrajs(dtrajs_split)
            # calculate count matrix on training data
            
            C = custom_count(dtrajs_train, lag=lag, mincount=mincount,
                             symmetric=symmetric)
            # avoid taking in empty states which throw error in row normalization
            C_visited = submatrix(C, sel=visited_set(C))
            # calculate T on training data
            tmat_train = transition_matrix(C_visited, **transition_params)
            # scoring
            s = score(dtrajs_train, dtrajs_test, tmat_train, symmetric=symmetric,
                      score_k=score_k, score_method=score_method, lag=lag,
                      mincount=mincount, is_tmat_active=True)
            scores.append(s)
        return np.array(scores)


def _numerical_scoring_test(dtraj_train: np.ndarray = None,
                            dtraj_test: np.ndarray = None,
                            tmat_train: np.ndarray = None,
                            score_method: str = "VAMP2",
                            mincount: int = 0,
                            lag: int = 1) -> bool:
    from pyemma.msm import estimate_markov_model

    if dtraj_train is None and dtraj_test is None and tmat_train is None:
        # Fully connected trajectories as default
        discrete_trajs = [np.array([1, 0, 1, 0, 2, 2, 2, 1, 1, 2, 0]),
                          np.array([2, 0, 2, 0, 1, 1, 2, 1, 1, 2, 0])]
        dtraj_train = discrete_trajs[0]
        dtraj_test = discrete_trajs[1]
        tmat_train = transition_matrix(custom_count(dtraj_train, lag, symmetric=False),
                                       reversible=False)

    elif not dtraj_train is not None and dtraj_test is not None and tmat_train is not None:
        raise ValueError("Provide either all or none of dtraj_train, dtraj_test, tmat_train.")

    else:
        C_train = custom_count(dtraj_train, lag=lag, symmetric=False)
        C_test = custom_count(dtraj_test, lag=lag, symmetric=False)
        
        # Test whether discrete_trajs are fully connected
        i, j = np.where((C_train == 0) & (C_train.T != 0))
        k, l = np.where((C_test == 0) & (C_test.T != 0))
        if any((i, j)) or any((k, l)):
            raise ValueError('Discrete trajectory is not fully connected.')
            
    custom_vamp = score(dtraj_train, dtraj_test, tmat_train, lag=lag, mincount=mincount,
                        score_method=score_method, score_k=None, symmetric=False)
    
    mm = estimate_markov_model(dtraj_train, lag=lag, reversible=False,
                               count_mode='sliding', sparse=False, maxiter=1000000,
                               connectivity='largest', dt_traj='1 step',
                               maxerr=1e-8, score_method=score_method, score_k=None,
                               mincount_connectivity=0, core_set=None,
                               milestoning_method='last_core')
    vamp_score = mm.score(dtraj_test)
    print(vamp_score, custom_vamp)
    
    return np.allclose(custom_vamp, vamp_score)


if __name__ == '__main__':
    # Use default trajectories for testing
    print(_numerical_scoring_test())
    # Supply trajectories externally for testing
    dtraj_train = np.array([1, 0, 0, 1])
    dtraj_test = np.array([1, 0, 0, 1])
    T = transition_matrix(custom_count(dtraj_train, mincount=0,
                                       symmetric=False, lag=1))
    print(_numerical_scoring_test(dtraj_train, dtraj_test, T))
    # Use crossvalidation
    dtrajs = [np.array([1, 0, 1, 0, 2, 2, 2, 1, 1, 2, 0, 2, 2, 1, 1, 2]),
              np.array([1, 0, 0, 0, 1, 2, 1, 1, 1, 2, 0, 2, 2, 1, 1, 2])]
    scores = score_cv(dtrajs, symmetric=False, lag=2)
