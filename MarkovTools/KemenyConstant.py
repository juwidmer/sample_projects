#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 12:33:26 2023

@author: jwidmer
"""
import numpy as np

"""Calculate Kemeny constant for a Markov chain."""


def get_cluster_weights(dtraj_all_lvls: np.ndarray, fwts, lvl: int = None):
    """Returns the weights of clusters for a given level. Cluster IDs refer
    to the ORIGINAL indexing in the dtraj."""
    if lvl:
        dtraj = dtraj_all_lvls[:, lvl-1]
    elif len(dtraj_all_lvls.shape) == 1:
        dtraj = dtraj_all_lvls
    # counts are N_i
    cluster, counts = np.unique(dtraj, return_counts=True)
    
    # Vector of MSM cluster weights
    pi_fw = np.ones_like(cluster) / dtraj.shape[0]
    
    # w_i = pi_i / Ni -> pi_i = w_i * Ni
    for i, cl in enumerate(cluster):
        idx = np.where(dtraj == cl)
        cl_idx = np.where(cluster == cl)
        # Multiply the raw count N_i from discrete traj with (first) w_i from the file
        # since all w_i for a given cluster are the same. Result is pi_i from the MSM
        pi_i = counts[cl_idx] * fwts[idx][0]
        pi_fw[i] *= pi_i
    
    return np.array((cluster.astype(int), pi_fw)).T


def kemeny(mfpt_dict, pi_dict):
    """The mfpt_dict maps the MFPT i->j the index of the SINK state, j.
    Indexing is based on ORIGINAL cluster indexing as given in the dtraj."""
    m = mfpt_dict
    p = pi_dict
    # All states have a pi_i but only connected component has an MFPT
    assert set(m.keys()).issubset(set(p.keys())) 
    K = 0
    for state in m.keys():
        K += m[state] * p[state]

    return K