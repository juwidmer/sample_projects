#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 15:47:10 2022

@author: jwidmer
"""
import numpy as np
import pandas as pd
from typing import List
"""Implement PCA on a torus: https://aip.scitation.org/doi/10.1063/1.4998259
Shift periodic data such that the boundary is in the least populated region
of a histogram. This permits finding a linear subspace that represents the data
accurately by minimizing boundary artifacts."""

pi = 3.1415
binw = 0.08726646 # 5 deg in radians


def find_minPop_bin(population: np.ndarray, edges: np.ndarray) -> int:
    """Find histogram bin with minimal population"""
    n = population.shape[0]
    m = population.min()

    di = 1 # which neighbour left and right to consider
    # initial candidates: all those bins where population is minimal
    candidates = np.where(population == m)[0]
    while len(candidates) > 1: # iterate until only one candidate ist left
        mn = 1e10 # initialize minimum value to high number
        adjacs = list() # initialize list of neighbours' summed population
        for bin_id in candidates: # iterate over candidates
            low, hi = (bin_id-di) % n, (bin_id+di) % n # needs to wrap around
            adjacent = population[low] + population[hi] # sum of adjacent populations
            adjacs.append(adjacent)
            if adjacent <= mn: # update minimum population
                mn = adjacent
        # update candidates which have the minimum population in their neighbours
        candidates = candidates[np.where(adjacs == min(adjacs))[0]]
        di += 1 # increase neighbourhood if there are still ties
    # Return bin with lowest density region
    minbin = candidates[0]
    return minbin


def find_leastDens(data: np.ndarray, bins: np.ndarray = None, **kwargs):
    """Shift origin of each dihedral to least densely populated region (5 deg bins)"""

    if bins is None:
        bins = np.arange(min(data), max(data), binw)
    population, edges = np.histogram(data, bins=bins, **kwargs)
    # Find minimum population bin with greedy tie break
    minbin = find_minPop_bin(population, edges)

    mp = (edges[minbin] + edges[minbin + 1])/2
    return mp, bins


def shift_df(df: pd.DataFrame, midpoints: List[float] = None):
    """Shifts each column of df to its minimum density region"""
    shifted_df = df.copy()
    mps = list()
    for i, col in enumerate(shifted_df.columns):
        data = shifted_df.loc[:, col]
        if midpoints is None:
            mp, bins = find_leastDens(data)
        else:
            mp = midpoints[i]
            bins = np.arange(min(data), max(data), binw)
        shift = pi - mp
        shifted_data = ((data + pi + shift) % (2*pi)) - pi
        shifted_df.loc[:, col] = shifted_data
        mps.append(mp)
    return shifted_df, bins, mps
