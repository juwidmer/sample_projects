
A sample of code generated over the course of my PhD work. The code consists mainly of self-contained modules or scripts that contain neat and creative ideas that were not published elsewhere. The directory ```courses``` contains my solutions of exercises for various courses I followed beyond those attended for ECTS. 

```bash
courses/
├── CompSysBio_stochasticApproaches
├── ProbabilisticAI
└── StatModelsCompBio
```

