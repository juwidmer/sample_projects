#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 27 17:19:04 2022

@author: julian
"""
from typing import Tuple
import numpy as np
## A
# Law of total variance:
# Total = Intrinsic + Extrinsic
# Var(N) = E(Var(N | k)) + Var(E(N | k))
gamma = 2
mu_Z = 3 # mean of poisson RV
baserate = 1 # non-stochastic contribution to k
k = baserate + np.random.poisson(mu_Z)
# stationary mean for a particular k; E(N | k) = k / gamma
# By linearity of expectation,  E(k / gamma) = E(k) / gamma.
# Moreover, E(k) = 1 + E(Z) = 1 + 3 = 4
# In Poisson, expectation = var.
# Therefore, E(Var(N | k)) = E(E(N | k)) = E(k / gamma) = E(k) / gamma = 4 / 2
intrinsic = (1 + mu_Z) / gamma
# Also, Var(E(N | k)) = Var(k / gamma)
# = Var(k) / gamma^2    [by constant gamma]
# = Var(1 + Z) / gamma^2
# = (Var(1) + Var(Z)) / gamma^2 [because 1 is constant]
# = 0 + Var(Z) / gamma^2
# = E(Z) / gamma^2 [by Z~Poisson(mu_Z)]
# = 3 / 4 = 0.75
extrinsic = mu_Z / gamma**2

Var_N = extrinsic + intrinsic
frac_extrinsic = extrinsic / Var_N
frac_intrinsic = intrinsic / Var_N

def simulate_birth_death(rate_generate: float, rate_degrade: float,
                         t_final: float) -> Tuple[float, int]:
    """Simulate a single trajectory of a birth-death-process.
    Degradation rate is rate_degrade * Ni for the N_i+1 step"""
    t = 0
    N = 0
    delta_t = 0

    jump_ts = [0] # initialize to 0
    Ns = [0] # initialize to 0
    while True:
        # First reaction method: Generate exponential waiting time of 
        # birth and death separately. Smaller time wins.
        delta_t_gen = np.random.exponential(scale=1/rate_generate)
        deg = rate_degrade * Ns[-1]
        delta_t_deg = 1e10
        if deg != 0:
            delta_t_deg = np.random.exponential(scale=1/deg)
        delta_t = min(delta_t_gen, delta_t_deg)
        if t + delta_t <= t_final:
            if delta_t_gen < delta_t_deg:
                N += 1
            elif delta_t_deg < delta_t_gen:
                N -= 1
            else: continue 

            t += delta_t
            jump_ts.append(t)
            Ns.append(N)
        else:
            break
    return jump_ts, Ns


def main(baserate: float, mu_Z: float):
    ## B
    print("BASERATE: ", baserate)
    print(r"$\mu_Z$: ", baserate)
    n_cells = 10000
    t_final = 50
    
    Ns = list()
    for cell_i in range(n_cells):
        rate_generate = baserate + np.random.poisson(mu_Z)
        _, Ns_i = simulate_birth_death(rate_generate=rate_generate,
                                       rate_degrade=gamma, t_final=t_final)
        Ns.append(np.array(Ns_i))
    
    var_final = np.array([ni[-1] for ni in Ns]).var()
    print('B:\nVariance total: ', var_final)

    ## C
    # Count of mRNA of gene A and gene B, which are expressed in the same cell
    Ns_A, Ns_B = list(), list()
    
    for cell_i in range(n_cells):
        # Roll a common birth rate for the two cells
        rate = baserate + np.random.poisson(mu_Z)
        
        _, Ns_i = simulate_birth_death(rate_generate=rate,
                                       rate_degrade=gamma, t_final=t_final)
        Ns_A.append(np.array(Ns_i))
        _, Ns_i = simulate_birth_death(rate_generate=rate,
                                       rate_degrade=gamma, t_final=t_final)
        Ns_B.append(np.array(Ns_i))
    
    
    final_A = np.array([ni[-1] for ni in Ns_A])
    final_B = np.array([ni[-1] for ni in Ns_B])
    covar_final = np.mean(final_A * final_B) - final_A.mean()*final_B.mean()
    extrinsic_frac = covar_final / var_final
    intrinsic_frac = 1 - extrinsic_frac
    print('C:\nIntrinsic: ', intrinsic_frac, 'Extrinsic: ', extrinsic_frac)
    #%%
    ## D
    n_cells = 10000
    t_final = 50
    
    Y_ts = list()
    Ns = list()
    
    for cell_i in range(n_cells):
        rate = baserate + np.random.poisson(mu_Z)
        jump_i, Ns_i = simulate_birth_death(rate_generate=rate,
                                       rate_degrade=gamma, t_final=t_final)
        Ns.append(Ns_i)
        # Manually add final time as the upper integration bound
        jump_i.append(50)
        # Numerically integrate
        Y_ti = 1/t_final * sum([(jump_i[i + 1] - jump_i[i]) * Ns_i[i] for i in range(len(Ns_i))])
        Y_ts.append(Y_ti)
    
    # Due to ergodicity, Var(X_t) = Var_tot = Var(Y_t) + intrinsic
    Var_Y = np.array(Y_ts).var()
    Var_tot = np.array([ni[-1] for ni in Ns]).var()
    extrinsic_frac = Var_Y / Var_tot
    intrinsic_frac = 1 - extrinsic_frac
    print('D:\nIntrinsic: ', intrinsic_frac, 'Extrinsic: ', extrinsic_frac)




if __name__ == '__main__':
    main(baserate=1, mu_Z=3)
    main(baserate=3, mu_Z=1)
