#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 21:09:39 2022

@author: julian
"""
import numpy as np
from scipy.linalg import eig, expm
import matplotlib.pyplot as plt
np.set_printoptions(precision=2)

X1 = 0 # protein copies: {0, 1, 2, 3, ...}
X2 = 0 # active gene copies: {0, 1}
n_dim = 5 # dimensionality of truncated state space
designated_state = (0, 0)
t_final = 10
Ns = range(1, 21)

Sm = np.array([[0, 0, 1, -1],  # protein copies
               [1, -1, 0, 0]]) # active gene copies


def calculate_pi(P):
    """Calculate normalized right eigenvector (e.g., stationary probability)"""
    eigenvalues, right_ev = eig(P, right=True)
    stat_idx = np.where(np.isclose(eigenvalues.real, 0.))[0][0]
    stationary = right_ev[:, stat_idx]/right_ev[:, stat_idx].sum()
    return stationary


def get_propesities(x1: int, x2: int) -> np.ndarray:
    # Gene activtion, gene deactivation, translation, protein degradation 
    return np.array([5*(1-x2)/(1 + x1), 2*x2, 10*x2, 3*x1])


def phi(x1: int, x2: int) -> int:
    return 2*x1 + x2


def construct_truncated_state_space(n1: set, n2: set, phi: callable) -> dict:
    """Use callable phi to invertibly assign an index to each state in state
    space n1 x n2."""
    S = dict()
    for i in n1:
        for j in n2:
            S[i, j] = phi(i, j)
    if len(set(S.values())) != len(S):
        e = f'Function {phi.__name__} is not invertible on n1={n1}, n2={n2}.'
        raise ValueError(e)
    return S


def construct_FSP_matrix(S: dict, Sm: np.ndarray) -> np.ndarray:
    """Construct finite state projection rate matrix of CME from state space S
    and stoichiometry matrix Sm."""
    n = len(S)
    A = np.zeros((n, n))
    for xi, i in S.items():
        for xj, j in S.items():
            # Propensities for system in state j; c.f. L7, slide 3.
            w = get_propesities(*xj)
            if i == j:
                A[i, j] = -w.sum()
                continue
            # index of reactions such that Xj goes to Xi
            rxn_ids = [rxn_i for rxn_i in range(Sm.shape[1])
                       if (xj + Sm.T[rxn_i] == xi).all()] # np converts tuple to array
            A[i, j] = w[rxn_ids].sum()
    return A


def to_stationary_FSP(A: np.ndarray, Sm: np.ndarray, S: dict,
                      designated_state: int) -> np.ndarray:
    """Redirect outgoing transitions to a designated state in order to 
    render the process encoded by the finite state projection matrix stationary."""
    cji = np.zeros(len(A)) # Out-flux vector
    # For every state, calculate the sum of propensities of reactions
    # in Sm that lead to a state that is not contained in S
    truncated_S = list(S.keys())
    for i, state in enumerate(S):
        # Take reaction index of Sm such that the result is not in truncated state space
        rxn_ids = [rxn_i for rxn_i in range(Sm.shape[1])
                   if not tuple(state + Sm.T[rxn_i]) in truncated_S]
        # Calculate sum of propensities; 'leakage' out of truncated state space
        cji[i] = get_propesities(*state)[rxn_ids].sum()
    # Vector with designated state
    e_l = np.zeros_like(cji)
    designated_index = S[designated_state]
    e_l[designated_index] = 1
    # Construct sFSP matrix
    A_bar = A + np.outer(e_l, cji)
    # Columns should sum to 0
    try:
        assert np.isclose(A_bar.sum(0), 0).all()
    except AssertionError:
        print(A)
        print(A_bar)
        raise
    return A_bar


def solve_FSP(A: np.ndarray, t_final: float, p0: np.ndarray) -> np.ndarray:
    p_t = expm(A*t_final) @ p0
    return p_t


def calc_FSP_error(p_t: np.ndarray) -> float:
    return 1 - p_t.sum()


def calc_sFSP_outflow(pi: np.ndarray) -> float:
    return 10*pi[-1]


def calc_FSP_projection_errors(r: iter):
    errors = []
    for n_dim in r:
        S = construct_truncated_state_space(n1=range(n_dim), n2={0, 1}, phi=phi)
        p0 = np.zeros(len(S))
        p0[S[(X1, X2)]] = 1.
        
        A = construct_FSP_matrix(S, Sm)
        p_t = solve_FSP(A, t_final, p0)
        errors.append(calc_FSP_error(p_t))
    return errors, S, p_t


def calc_sFSP_projection_errors(r: iter):
    outflows = []
    for n_dim in r:
        S = construct_truncated_state_space(n1=range(n_dim), n2={0, 1}, phi=phi)
        p0 = np.zeros(len(S))
        p0[S[(X1, X2)]] = 1.
        
        A = construct_FSP_matrix(S, Sm)
        A_bar = to_stationary_FSP(A, Sm, S, designated_state=designated_state)
        stationary_p = calculate_pi(A_bar).real
        outflows.append(calc_sFSP_outflow(stationary_p))
    return outflows, S, stationary_p

#%%
# A)
S = construct_truncated_state_space(n1=range(n_dim), n2={0, 1}, phi=phi)
p0 = np.zeros(len(S))
p0[S[(X1, X2)]] = 1.
A = construct_FSP_matrix(S, Sm)

print(A)
# B)
A_bar = to_stationary_FSP(A, Sm, S, designated_state=designated_state)

print(A_bar)
#%%
# C)
errors, S20, p_t20 = calc_FSP_projection_errors(Ns)
# D)
outflows, S_pi, stationary_p = calc_sFSP_projection_errors(Ns)
#%%
### Plot errors of FSP
fig, ax = plt.subplots()
    
ax.bar(Ns, errors)
ax.set_xticks(Ns)
ax.set_yscale('log')
ax.set_xlabel('N')
ax.set_title('FSP error as a function of truncation of X1')
ax.set_ylabel('$\epsilon_n$(10)')
fig.tight_layout()
fig.savefig('FSP_errors.png', dpi=300)
#%%
### Plot FSP marginals
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.bar(Ns, [p_t20[i:i+2].sum() for i in Ns])
ax1.set_xlabel('protein copies')
ax1.set_ylabel('probability mass')
fig.suptitle('Marginal distributions of FSP\nat $t = 10$ for $n = 20$')

ax2.bar([0, 1], [p_t20[::2].sum(), p_t20[1::2].sum()])
ax2.set_ylabel('probability mass')
ax2.set_xlabel('active gene')
ax2.set_xticks([0, 1])
fig.tight_layout()
fig.savefig('FSP_marginals.png', dpi=300)
#%%
### Plot sFSP outflow rate
fig, ax = plt.subplots()
    
ax.bar(Ns, outflows)
ax.set_xticks(Ns)
ax.set_yscale('log')
ax.set_xlabel('N')
ax.set_title('sFSP outflow rate as a function of truncation of X1')
ax.set_ylabel('$\epsilon_n$(10)')
fig.tight_layout()
fig.savefig('sFSP_outflows.png', dpi=300)
#%%
### Plot FSP marginals
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.bar(Ns, [stationary_p[i:i+2].sum() for i in Ns])
ax1.set_xlabel('protein copies')
ax1.set_ylabel('probability mass')
fig.suptitle('Marginal stationary distributions of sFSP for $n = 20$')

ax2.bar([0, 1], [stationary_p[::2].sum(), stationary_p[1::2].sum()])
ax2.set_ylabel('probability mass')
ax2.set_xlabel('active gene')
ax2.set_xticks([0, 1])
fig.tight_layout()
fig.savefig('sFSP_stationary_marginals.png', dpi=300)