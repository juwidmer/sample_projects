#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 21:09:39 2022

@author: julian
"""
import numpy as np
from scipy.stats import poisson
from scipy.linalg import eig
import matplotlib.pyplot as plt
np.set_printoptions(precision=4)

ks = [1, 2, 5]
gamma = 1
X0 = 0
n_max = 11

def construct_A_n(n_max: int, k: float, gamma: float):
    """Return finite state projection matrix for a birth-death-process with
    parameters k and gamma. n_max is the dimensionality of the matrix."""
    A_n = np.zeros((n_max, n_max))
    for n in range(n_max):
        A_n[n, n] = -(k + n * gamma)
        try: # for n == n_max, only change diagonal element
            A_n[n, n+1] = (n + 1) * gamma
            A_n[n+1, n] = k
        except IndexError:
            pass
    return A_n


def calculate_pi(P):
    eigenvalues, right_ev = eig(P, right=True)
    stat_idx = np.where(np.isclose(eigenvalues.real, 0.))[0][0]
    stationary = right_ev[:, stat_idx]/right_ev[:, stat_idx].sum()
    return stationary



#%%
fig, ax  = plt.subplots(3, 1, sharex=True, sharey=True, figsize=(4, 6))
fig_e, ax_e = plt.subplots()
w = 0.4
for i, k in enumerate(ks):
    A = construct_A_n(n_max, k, gamma)
    # Direct outgoing transitions of the truncated state space to state 0 with rate k
    A[0, -1] = k
    stationary_Py = calculate_pi(A)
    
    out_flow = k*stationary_Py[-1]
    ax_e.bar(i, out_flow, color='tab:blue')

    p = k/gamma
    x = np.arange(poisson.ppf(0.001, p),
                  poisson.ppf(0.99999, p))
    reference_pmf = poisson.pmf(x, p)
    ax[i].set_xticks(x)
    ax[i].bar(x-w, reference_pmf, align='edge', width=w, label='Poisson', alpha=0.8)
    ax[i].bar(np.arange(len(stationary_Py)), stationary_Py, align='edge', width=w,
              label='sFSP', alpha=0.8)
    ax[i].set_title(f'k/γ = {p}')
    # ax[i].set_yscale('log')
ax[0].legend()
ax[i].set_xlabel('X')
ax[1].set_ylabel('Probability Mass')
fig.suptitle('Stationary distributions')
fig.tight_layout()
fig.savefig('ex6_1_1.png', dpi=300)

ax_e.set_yscale('log')
ax_e.set_xticks(range(len(ks)))
ax_e.set_xticklabels(ks)
ax_e.set_xlabel('k/γ')
ax_e.set_ylabel('Outflow')
ax_e.set_title('Outflow rate for sFSP')
fig_e.tight_layout()
fig_e.savefig('ex6_1_2.png', dpi=300)
