#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 21:59:18 2022

@author: julian
"""

import numpy as np
import matplotlib.pyplot as plt

def simulate_birth_death_RTC(k: float, gamma: float, t_final: float):
    """Simulate a single trajectory of a birth-death-process by Random Time Change
    method. Generate two stopping times from single exponential for birth/death each.
    For birth, it is scaled by k. For death, it is scaled by gamma \int_0^t X ds.
    Overall, X(t) = X(0) + Y_b(kt) + Y_d(gamma \int_0^t X ds), where Y ~ Poisson(1).
    Y_b and Y_d are independent."""
    t = 0
    X = 0
    
    jump_ts = [0] # initialize to 0
    Xs = [0] # initialize to 0

    while True:
        # Get incorrect results if not both are rerolled at each iteration
        delta_t_gen = np.random.exponential(scale=1)
        delta_t_deg = np.random.exponential(scale=1)
        # Convert to "internal time": Generate two stopping times from
        # single exponential for birth/death each. For birth, it is scaled by k.
        dt_gen_internal = delta_t_gen / k
        # For death, it is scaled by 1 / (gamma * X_{i-1})
        try:
            dt_deg_internal = delta_t_deg / (gamma * X)
        except ZeroDivisionError:
            dt_deg_internal = 1e10
        # increment original time 
        delta_t = min(dt_gen_internal, dt_deg_internal)
        # check if t_final is not exceeded
        if t + delta_t <= t_final:
            # Check which one fires first in internal time
            # Birth
            if dt_gen_internal < dt_deg_internal:
                X += 1
            # Death
            elif dt_deg_internal < dt_gen_internal:
                X -= 1
            # update
            t += delta_t
            jump_ts.append(t)
            Xs.append(X)
        else:
            break
    return jump_ts, Xs


def get_moments(array):
    return np.mean(array), np.var(array), np.sqrt(np.var(array))/np.mean(array)

k = 2
gamma = 1
t_final = 10
n_cells = 10000

jumps = list()
Ns = list()
for path_i in range(n_cells):
    jumps_i, Ns_i = simulate_birth_death_RTC(k=k, gamma=gamma, t_final=t_final)
    jumps.append(np.array(jumps_i))
    Ns.append(np.array(Ns_i))


# fig, ax = plt.subplots()
# for jumps_i, Ns_i in zip(jumps[:5], Ns[:5]):
#     ax.step(jumps_i, Ns_i)


#%%
# Take number of mRNA at the last time < 2 (< 5)
at_t2 = [Ns_i[np.where(jumps_i < 2)[0][-1]] for jumps_i, Ns_i in zip(jumps, Ns)]
at_t5 = [Ns_i[np.where(jumps_i < 5)[0][-1]] for jumps_i, Ns_i in zip(jumps, Ns)]
# Last time point of each sample path
at_t10 = [Ns_i[-1] for Ns_i in Ns]

m2, s2, c2 = get_moments(at_t2)
m5, s5, c5 = get_moments(at_t5)
m10, s10, c10 = get_moments(at_t10)

#%%
fig, ax = plt.subplots(figsize=(6, 4))
bins = range(max(at_t10)+1)
w = 0.8
a = 0.5
n2, _, _ = ax.hist(at_t2, alpha=a, bins=bins, width=w, density=True, label=rf't=2: $\mu$={m2:.1f}, $\sigma^2$={s2:.1f}, cv={c2:.1f}')
n5, _, _ = ax.hist(at_t5, alpha=a, bins=bins, width=w, density=True, label=rf't=5: $\mu$={m5:.1f}, $\sigma^2$={s5:.1f}, cv={c5:.1f}')
n10, _, _ = ax.hist(at_t10, alpha=a, bins=bins, width=w, density=True, label=rf't=10: $\mu$={m10:.1f}, $\sigma^2$={s10:.1f}, cv={c10:.1f}')
mx = max(at_t10)
ax.legend(title='Direct estimate:')
ax.set_xticks([i+w/2 for i in range(mx)])
ax.set_xticklabels(range(mx))
ax.set_xlabel('N')
ax.set_ylabel('Probability mass')
ax.set_title(rf'RTC birth-death-process: k={k}, $\gamma$={gamma}')
fig.tight_layout()
fig.savefig('ex4_1.pdf', dpi=300)
