#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 19:06:31 2022

@author: julian
"""
import time
import numpy as np
#              c1 c2 c3  c4  c5  c6
S = np.array([[-1, 1, -2, 2, 1, -1], # X
              [1, -1, 0, 0, 0, 0], # Y
              [0, 0, 1, -1, 0, 0], # Z
              [0, 0, 0, 0, -1, 1]]) # W

c = [1, 1, 2, 2, 0.1, 0.1]
#               X, Y, Z, W
X0 = np.array([20, 0, 0, 10])

def timing(f):
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = f(*args, **kwargs)
        time2 = time.time()
        print(f'{kwargs["method"]} function took {(time2-time1):.3f} s')
        return ret
    return wrap


def not_implemented(method: str) -> None:
    print(f'{method} is not implemented.')


def update_propensities(c, X):
    w_ = np.array([X[0], X[1], X[0]*(X[0]-1)/2, X[2], X[0]*X[3], X[0]*(X[0]-1)/2])
    w = w_ * c
    return w


def get_moments(array) -> tuple:
    mean, var = np.mean(array, axis=0), np.var(array, axis=0)
    return mean, var, np.sqrt(var)/mean


def Gillespie(S, c, X0, t_final: float) -> np.ndarray:
    t = 0
    X = np.array(X0)
    while True:
        w = update_propensities(c, X)
        w0 = w.sum()
        dt = np.random.exponential(1/w0)
        i = np.random.multinomial(1, w/w0).argmax()
        if t + dt <= t_final:
            X += S[:, i]
            t += dt
        else:
            break
    return X


def FirstReaction(S, c, X0, t_final: float) -> np.ndarray:
    t = 0
    X = np.array(X0)
    
    while True:
        w = update_propensities(c, X)
        dts = [np.random.exponential(1/w_k) for w_k in w]
        dt = min(dts)
        i = np.argmin(dts)
        if t + dt < t_final:
            X += S[:, i]
            t += dt
        else:
            break
    return X


def tauLeaping(S, c, X0, t_final: float, tau: float) -> np.ndarray:
    t = 0
    X = np.array(X0)
    while True:
        w = update_propensities(c, X)
        Nk = np.random.poisson(w*tau) # Vector of how often each reaction happens
        if t + tau < t_final:
            t += tau
            X += (S * Nk).sum(axis=1)
            X = X.clip(0) # Cannot be negative, so clip to 0
        else:
            break
    return X


def mNRM(S, c, X0, t_final: float) -> np.ndarray:
    t = 0
    X = np.array(X0)
    Sm = np.zeros_like(c)
    Tm = np.random.exponential(scale=1, size=len(c))
    # u = np.random.uniform(size=len(c))
    # Tm = -np.log(u)
    while True:
        w = update_propensities(c, X)
        delta_m = (Tm - Sm) / w
        i = np.argmin(delta_m)
        dt = delta_m[i]
        if t + dt < t_final:
            t += dt
            X += S[:, i]
            Sm += w*dt
            Tm += np.random.exponential(scale=1, size=len(c))
        else:
            break
    return X


@timing
def simulate(S, c, X0, t_final: float, n_path: int, method: str, **kwargs) -> list:
    
    methods = {'Gillespie': Gillespie, 'Firstreaction': FirstReaction,
               'Tauleaping': tauLeaping, 'Mnrm': mNRM}
    func = methods.get(method.capitalize())
    if func is None:
        not_implemented(method)
        return
    X_ = []
    for i in range(n_path):
        xi = func(S, c, X0, t_final=t_final, **kwargs)
        X_.append(xi)
    return X_




def main(n_path, t_final):
    X_gillespie = simulate(S, c, X0, t_final, n_path=n_path, method='Gillespie')
    X_frm = simulate(S, c, X0, t_final, n_path=n_path, method='FirstReaction')
    X_tauleap = simulate(S, c, X0, t_final, n_path=n_path, method='TauLeaping', tau=0.01)
    X_mNRM = simulate(S, c, X0, t_final, n_path=n_path, method='mNRM')
    X_tauleap_fast = simulate(S, c, X0, t_final, n_path=n_path, method='TauLeaping',
                              tau=0.1)
    
    moments_gillespie = get_moments(X_gillespie)
    moments_frm = get_moments(X_frm)
    moments_tauleap = get_moments(X_tauleap)
    moments_mnrm = get_moments(X_mNRM)
    moments_tauleap_fast = get_moments(X_tauleap_fast)
    return moments_gillespie, moments_frm, moments_tauleap, moments_mnrm, moments_tauleap_fast

if __name__ == '__main__':
    # results = main(n_path=10000, t_final=1)
    n_path=10000
    t_final=1
    X_mNRM = simulate(S, c, X0, t_final, n_path=n_path, method='mNRM')
    moments_mnrm = get_moments(X_mNRM)
    print(moments_mnrm)