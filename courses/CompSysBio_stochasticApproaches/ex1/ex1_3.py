#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  6 22:49:21 2022

@author: julian
"""

# 3. The goal of this exercise is to simulate a Poisson process {N (t) : t ≥ 0} with rate λ,
# according to its inter-arrival times, and observe that for any time t, N (t) is a Poisson
# random variable with parameter λt. Recall that this process starts at 0 (i.e. N (0) = 0)
# and it jumps up by 1 at times T 1 , (T 1 + T 2 ), (T 1 + T 2 + T 3 ), . . . , where each T i is an
# independent exponential random variable with rate λ.

# (A) Viewing the rate-λ Poisson process as a continuous time Markov chain (CTMC),
# write its generator matrix A. This matrix will be infinite and you just need to
# provide the general form of its rows/columns.

# (B) Simulate 5 paths of the Poisson process with λ = 10 in the time interval [0, 10].
# Plot these paths in the same figure.

# (C) Now simulate 1000 paths of the Poisson process {N (t) : t ≥ 0} with λ = 10 in the
# time interval [0, 5]. This will give you 1000 realizations of the random variable
# N (5). Plot these realizations in a histogram with bins at integers 0, 1, 2, . . . , 100.
# For the purpose of comparison, also plot the p.m.f. of the Poisson distribution
# with parameter 50 (use the function poisspdf in Matlab).

import numpy as np
from scipy.stats import poisson
import matplotlib.pyplot as plt
from typing import Tuple

def simulate_poisson(rate: float, t_final: float) -> Tuple[float, int]:
    t = 0
    N = 0
    
    jump_ts = list()
    Ns = list()
    while True:
        u = np.random.uniform()
        delta_t = - np.log(1 - u) / rate
        if t + delta_t <= t_final:
            t += delta_t
            N += 1
            jump_ts.append(t)
            Ns.append(N)
        else:
            break
    return jump_ts, Ns
#%%
# (B)
n_paths = 5
l = 10
t_final = 10

jump_times = list()
N_s = list()

for n in range(n_paths):
    jump_times_i, ni = simulate_poisson(rate=l, t_final=t_final)
    jump_times.append(jump_times_i)
    N_s.append(ni)


fig, ax = plt.subplots()
for jump_i, ni in zip(jump_times, N_s):
    ax.step(jump_i, ni)

ax.set_xlabel('t')
ax.set_ylabel('N')
ax.set_title(f'Poisson process λ = {l}')

fig.tight_layout()
fig.savefig('ex1_3b', dpi=300)

#%%
# (C)
n_paths = 1000
l = 10
t_final = 5

N_s = list()

for n in range(n_paths):
    _ , ni = simulate_poisson(rate=l, t_final=t_final)
    N_s.append(ni[-1])

x = np.arange(poisson.ppf(0.01, l*t_final),
              poisson.ppf(0.99, l*t_final))
reference_pmf = poisson.pmf(x, l*t_final)


fig, ax = plt.subplots()

bins = range(101)
ax.hist(N_s, bins=bins, density=True, label='simulated', alpha=0.8)
ax.bar(x, reference_pmf, align='center', label=f'Poisson(λ = {l*t_final})', alpha=0.8,
       color='orange')

ax.set_xlabel('N')
ax.set_ylabel('Probability Mass')
ax.legend()
ax.set_title(f'Poisson process\nλ = {l}; t = {t_final}; n = {n_paths}')

fig.tight_layout()
fig.savefig('ex1_3c', dpi=300)