#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  6 21:50:31 2022

@author: julian
"""

# 2. This is the first Matlab exercise of this course. It illustrates how the Binomial distrib-
# uton can arise naturally in Biology.


# Consider a cell C which contains m plasmids. At some time, the cell C divides into two
# cells C 1 and C 2 . At the time of division, each plasmid can go to C 1 with probability
# p and to C 2 with probability (1 − p). Assume that all plasmids act independently of
# each other. Let X be the random variable that gives the number of plasmids that end
# up in cell C 1 after cell division. Clearly X is a discrete random variable with range
# R(X) = {0, 1, . . . , m}. Our goal is to numerically show that the distribution of X is
# Binomial with parameters m and p.

# (A) Let m = 10 and p = 0.3. Write a Matlab code to simulate this experiment and
# obtain a realization of the random variable X. For this you will have to generate
# 10 random variables u 1 , . . . , u 10 which are uniformly distributed over [0, 1]. Then
# X is just the number of these uniform random variables that fall below p.

# (B) Using the Matlab code in part (A), obtain 10000 realizations of the random vari-
# able X. Plot these realizations in a histogram. For the purpose of comparison,
# also plot the p.m.f. of the Binomial distribution with parameters m and p (use
# the function binopdf in Matlab)

import numpy as np
from scipy.stats import binom
import matplotlib.pyplot as plt
p = 0.3
m = 10

# (A)

def simulate_division(n_plasmids: int, p: float) -> int:
    """Returns random variable indicating the number of plasmids ending
    up in cell 1, given an initial number of plasmids in the parent cell
    and a probability p to be distributed to cell 1 upon division."""
    X = 0
    
    for m_i in range(m):
        roll = np.random.uniform()
        if roll < p:
            X += 1
    return X

# (B)

n_realizations = 10000
X_s = np.zeros(n_realizations)

for n in range(n_realizations):
    xi = simulate_division(n_plasmids=m, p=p)
    X_s[n] = xi

x = np.arange(binom.ppf(0.01, m, p),
              binom.ppf(0.99, m, p))
reference_pmf = binom.pmf(x, m, p)
#%%
fig, ax = plt.subplots()
hist_bins = range(m)

bar_width=0.2
ax.hist(X_s, label='simulated', density=True, alpha=0.8, bins=hist_bins,
        align='mid', width=bar_width)
ax.bar(x-bar_width, reference_pmf, align='edge', label=f'binom(n={m}, p={p})',
       color='orange', width=bar_width)

ax.set_xticks(range(m + 1))
ax.set_xlabel('X')
ax.set_ylabel('Probability Mass')
ax.legend()
fig.tight_layout()
fig.savefig('ex1_2.png', dpi=300)
