#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 23:07:53 2022

@author: julian
"""
import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple

k = 2
gamma = 1
p = 0.3

## A

def simulate_bursty_birth_death(rate_generate: float, rate_degrade: float,
                                t_final: float, p_generate_N: float) -> Tuple[float, int]:
    t = 0
    N = 0
    delta_t = 0

    jump_ts = [0] # initialize to 0
    Ns = [0] # initialize to 0
    while True:
        u = np.random.uniform()
        v = np.random.uniform()
        delta_t_gen = - np.log(1 - u) / rate_generate
        delta_t_deg = - np.log(1 - v) / (rate_degrade * Ns[-1]) # scale by Xi
        delta_t = min(delta_t_gen, delta_t_deg)
        if t + delta_t <= t_final:
            if delta_t_gen < delta_t_deg:
                # Now, N is distributed geometrically
                N += np.random.geometric(p_generate_N)
            elif delta_t_deg < delta_t_gen:
                N -= 1
            else: continue 

            t += delta_t
            jump_ts.append(t)
            Ns.append(N)
        else:
            break
    return jump_ts, Ns


#%%
## B
n_paths = 10000
jumps = list()
Ns = list()
for path_i in range(n_paths):
    jumps_i, Ns_i = simulate_bursty_birth_death(rate_generate=k, rate_degrade=gamma,
                                                t_final=10, p_generate_N=p)
    jumps.append(np.array(jumps_i))
    Ns.append(np.array(Ns_i))


at_t10 = [Ns_i[-1] for Ns_i in Ns]
m10 = np.mean(at_t10)
s10 = np.var(at_t10)
c10 = s10**0.5/m10

#%%
fig, (ax1, ax) = plt.subplots(1, 2, figsize=(9, 4))
m = max(at_t10)+1
bins = range(m)
w = 0.8
for jumps_i, Ns_i in zip(jumps[:5], Ns[:5]):
    ax1.step(jumps_i, Ns_i)

ax1.set_xlabel('time')
ax1.set_ylabel('N')

ax.hist(at_t10, alpha=0.7, density=True, bins=bins, width=w,
        label=rf't=10: $\mu$={m10:.1f}, $\sigma^2$={s10:.1f}, cv={c10:.1f}')
ax.set_xticks([i+w/2 for i in range(m)[::3]])
ax.set_xticklabels(range(m)[::3])

ax.legend()
ax.set_xlabel('N')
ax.set_ylabel('Probability mass')
fig.suptitle(f'Bursty birth-death process\nn={n_paths}, k={k}, $\gamma$={gamma}, p={p}')
fig.tight_layout()
fig.savefig('ex2_3.png', dpi=300)


