#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 14:02:17 2022

@author: julian
"""
import numpy as np
from scipy.linalg import expm
import matplotlib.pyplot as plt
# State space
S = range(11)
# initial condition: P(N = i) = 1 if i=0, 0 otherwise
p0 = np.zeros(len(S))
p0[0] = 1.
# parameters
k = 2
gamma = 1


def construct_A_n(n_max: int, k: float, gamma: float):
    A_n = np.zeros((n_max, n_max))
    for n in range(n_max):
        A_n[n, n] = -(k + n * gamma)
        try: # for n == n_max, only change diagonal element
            A_n[n, n+1] = (n + 1) * gamma
            A_n[n+1, n] = k
        except IndexError:
            pass
    return A_n


def calc_Pt(A: np.ndarray, t: float, p0: np.ndarray):
    return expm(A*t) @ p0


def calc_error(pt: np.ndarray):
    et = 1 - pt.sum()
    return et


A_n = construct_A_n(len(S), k, gamma)

p2 = calc_Pt(A_n, t=2, p0=p0)
p5 = calc_Pt(A_n, t=5, p0=p2)
p10 = calc_Pt(A_n, t=10, p0=p5)
#%%
fig, ax = plt.subplots()
a = 0.75
w = 0.3
ax.bar(np.array(S)-w, p2, width=w, alpha=a, label=f't=2, $\epsilon$={calc_error(p2):.4f}')
ax.bar(np.array(S), p5, width=w, alpha=a, label=f't=5, $\epsilon$={calc_error(p5):.4f}')
ax.bar(np.array(S)+w, p10, width=w, alpha=a, label=f't=10, $\epsilon$={calc_error(p10):.4f}')
ax.set_xticks(S)
ax.set_xlabel('N')
ax.set_ylabel('Probability mass')
ax.set_title(f'Finite state projection of birth-death-process\nk={k}, $\gamma$={gamma}')
ax.legend()
fig.tight_layout()
fig.savefig('ex2_2.png', dpi=300)
