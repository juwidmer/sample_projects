#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 21:54:56 2022

@author: julian
"""
import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple

k = 2
gamma = 1
n_paths = 10000
t_final = 10

def simulate_birth_death(rate_generate: float, rate_degrade: float,
                         t_final: float) -> Tuple[float, int]:
    t = 0
    N = 0
    delta_t = 0

    jump_ts = [0] # initialize to 0
    Ns = [0] # initialize to 0
    while True:
        u = np.random.uniform()
        v = np.random.uniform()
        delta_t_gen = - np.log(1 - u) / rate_generate
        delta_t_deg = - np.log(1 - v) / (rate_degrade * Ns[-1])
        delta_t = min(delta_t_gen, delta_t_deg)
        if t + delta_t <= t_final:
            if delta_t_gen < delta_t_deg:
                N += 1
            elif delta_t_deg < delta_t_gen:
                N -= 1
            else: continue 

            t += delta_t
            jump_ts.append(t)
            Ns.append(N)
        else:
            break
    return jump_ts, Ns


def calc_moment(n, pn, power: float):
    return sum([ni**power * pni for ni, pni in zip(n, pn)])


jumps = list()
Ns = list()
for path_i in range(n_paths):
    jumps_i, Ns_i = simulate_birth_death(rate_generate=k, rate_degrade=gamma,
                                         t_final=10)
    jumps.append(np.array(jumps_i))
    Ns.append(np.array(Ns_i))


fig, ax = plt.subplots()
for jumps_i, Ns_i in zip(jumps[:5], Ns[:5]):
    ax.step(jumps_i, Ns_i)


#%%
# Take number of mRNA at the last time < 2 (< 5)
at_t2 = [Ns_i[np.where(jumps_i < 2)[0][-1]] for jumps_i, Ns_i in zip(jumps, Ns)]
at_t5 = [Ns_i[np.where(jumps_i < 5)[0][-1]] for jumps_i, Ns_i in zip(jumps, Ns)]
# Last time point of each sample path
at_t10 = [Ns_i[-1] for Ns_i in Ns]

m2 = np.mean(at_t2)
m5 = np.mean(at_t5)
m10 = np.mean(at_t10)

s2 = np.var(at_t2)
s5 = np.var(at_t5)
s10 = np.var(at_t10)

c2 = s2**0.5/m2
c5 = s5**0.5/m5
c10 = s10**0.5/m10
#%%
fig, ax = plt.subplots(figsize=(6, 6))
bins = range(max(at_t10)+1)
w = 0.8
a = 0.5
n2, _, _ = ax.hist(at_t2, alpha=a, bins=bins, width=w, density=True, label=rf't=2: $\mu$={m2:.1f}, $\sigma^2$={s2:.1f}, cv={c2:.1f}')
n5, _, _ = ax.hist(at_t5, alpha=a, bins=bins, width=w, density=True, label=rf't=5: $\mu$={m5:.1f}, $\sigma^2$={s5:.1f}, cv={c5:.1f}')
n10, _, _ = ax.hist(at_t10, alpha=a, bins=bins, width=w, density=True, label=rf't=10: $\mu$={m10:.1f}, $\sigma^2$={s10:.1f}, cv={c10:.1f}')

ax.legend(title='Direct estimate:')
ax.set_xticks([i+w/2 for i in range(11)])
ax.set_xticklabels(range(11))
ax.set_xlabel('N')
ax.set_ylabel('Probability mass')
ax.set_title(rf'Birth-death process: k={k}, $\gamma$={gamma}')


m2 = calc_moment(set(at_t2), n2, 1)
m5 = calc_moment(set(at_t5), n5, 1)
m10 = calc_moment(set(at_t10), n10, 1)

s2 = calc_moment(set(at_t2), n2, 2) - m2**2
s5 = calc_moment(set(at_t5), n5, 2) - m5**2
s10 = calc_moment(set(at_t10), n10, 2) - m10**2

c2 = s2**0.5/m2
c5 = s5**0.5/m5
c10 = s10**0.5/m10

mom_string = f'Moment-based:\nt=2: $\mu$={m2:.1f}, $\sigma^2$={s2:.1f}, cv={c2:.1f}\n\
t=5: $\mu$={m5:.1f}, $\sigma^2$={s5:.1f}, cv={c5:.1f}\n\
t=10: $\mu$={m10:.1f}, $\sigma^2$={s10:.1f}, cv={c10:.1f}'
ax.text(0., -0.25, mom_string)
fig.tight_layout()
fig.savefig('ex2_1.png', dpi=300)






