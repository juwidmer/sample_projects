#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 14:32:47 2023

@author: jwidmer
"""

import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def generate_sinusoid_data(num_data):
    # Generate sinusoidal data with a gap in input space
    x_data = 10 * np.pi * (np.random.uniform(size=(4 * num_data // 2, 1)) - 0.5)
    x_data = np.sort(x_data, axis=0)
    x_data = np.concatenate([x_data[:(x_data.shape[0]//4)],
                             x_data[-(x_data.shape[0]//4):]],
                            axis=0)
    
    y_data = np.sin(x_data) + 1 + 1e-1 * np.random.normal(size=x_data.shape)
    return x_data, y_data


def gaussian_kernel(x: np.ndarray, xprime: np.ndarray, label_noise: float,
                    lengthscale: float, coeff: float):
    
    diff = x[:, None, :] - xprime[None, :, :]

    l2 = np.sum((diff / lengthscale) ** 2, axis=2)
    cov = coeff ** 2 * np.exp(-0.5 * l2)
    if label_noise is not None:
        cov = cov + label_noise ** 2 * np.eye(cov.shape[0])
    return cov


def exact_gp_posterior(x_data, y_data, x_pred, kernel, label_noise,
                       mu_prior = None, **kwargs):
    
    # x are the test points with no associated y, A is the set of training
    # points for which y is observed
    K_AA = kernel(x_data, x_data, label_noise, **kwargs)
    K_xA = kernel(x_pred, x_data, None, **kwargs)
    K_xx = kernel(x_pred, x_pred, None, **kwargs)

    # Usually, mean functions are defined as mu(xi) = 0
    mu_x = 0
    mu_A = 0
    y = (y_data[:, 0] - mu_A)

    KAA_inv = np.linalg.inv(K_AA)

    mu_ = mu_x +  K_xA @ KAA_inv @ y
    K_ = K_xx - K_xA @ KAA_inv @ K_xA.T

    return mu_, K_


def sample_posterior(mu, K, n_samples: int = 1):
    
    y_samples = np.random.multivariate_normal(mu, K, size=n_samples).T
    
    return y_samples


def RandomFourierFeatures(x_data, lengthscale, coeff, n_samples, ndim_fourier_feats):
    """https://random-walks.org/content/misc/rff/rff.html"""
    x_dim = x_data.shape[-1]
    omega_shape = (n_samples, ndim_fourier_feats, x_dim)
    # For Gaussian kernel only
    # sample rho (p.85, eq. (4.52))
    omega = np.random.normal(size=omega_shape) / lengthscale
    # Draw random frequencies
    weights = np.random.normal(loc=0.,  scale=1., 
                               size=(n_samples, ndim_fourier_feats))
    # sample b (p. 85, eq. (4.53))
    b = np.random.uniform(low=0.,  high=(2 * np.pi),
                          size=(n_samples, ndim_fourier_feats, 1))
    # Function z(x) defined on p.86, eq (4.53)
    features = 2 * np.cos(np.einsum('sfd, nd -> sfn', omega, x_data) + b)
    # eq. (4.56)
    features = (1 / ndim_fourier_feats) ** 0.5 * features * coeff
    
    functions = np.einsum('sf, sfn -> sn', weights, features)

    return functions, features


def rff_posterior(x_data, y_data, x_pred, lengthscale, coeff, ndim_fourier_feats, noise):
    """https://random-walks.org/content/misc/rff/rff.html"""
    num_data = x_data.shape[0]
    x_full = np.concatenate([x_pred, x_data])
    
    _, features = RandomFourierFeatures(x_data=x_full, 
                                        lengthscale=lengthscale, 
                                        coeff=coeff, 
                                        n_samples=1, 
                                        ndim_fourier_feats=ndim_fourier_feats)
    features = features[0]
    
    features_pred = features[:, :-num_data]
    features_data = features[:, -num_data:]
    
    ### ??? Is this really the covariance matrix? Probably it is.
    iS = np.eye(features_data.shape[0]) + features_data @ features_data.T * noise ** -2

    mean_pred = noise ** -2 * features_pred.T @ np.linalg.solve(iS, features_data @ y_data)[:, 0]
    
#   Equal to (features_pred * np.linalg.solve(iS, features_pred)).sum(axis=0)
    var_pred = np.einsum('fn, fn -> n',
                          features_pred,
                          np.linalg.solve(iS, features_pred))
    
    return mean_pred, var_pred, iS



def Nystrom_approx(x_data: np.ndarray, kernel: callable, label_noise, m: int = None, 
                   x_repr: np.ndarray = None, approx_inv: bool = False, **kwargs):
    """https://andrewcharlesjones.github.io/journal/nystrom-approximation.html"""
    # Randomly select m points in the original data set, which are considered
    # to be "inducing points" or "representative points".
    # One can also choose x data not in the data set,
    # for example an an even spacing of the x-range
    assert m or x_repr

    if x_repr is None:
        rng = default_rng()
        idx = np.sort(rng.choice(x_data.shape[0], m, replace=False))
        x_repr = x_data[idx]

    ### ??? Not sure whether label noise should be added to K11 or K22_approx
    K11 = kernel(x_repr, x_repr, label_noise=None, **kwargs)
    # SVD
    Lam, U = np.linalg.eigh(K11)
    # Similarity of full data with the representative points
    K21 = kernel(x_data, x_repr, label_noise=None, **kwargs)
    # Calculate covariance of projections. K12 = K21.T
    # Storage cost: O(nm) instead of O(n^2)
    K22_approx = K21 @ U @ np.diag(1 / Lam) @ U.T @ K21.T
    if label_noise is not None:
        K22_approx = K22_approx + label_noise ** 2 * np.eye(K22_approx.shape[0])

    ### ??? How to use the rapidly calculated inverse? This seems wrong
    ## Inversion using Woodbury identity of K_approx. Computational cost: O(nm^2)
    if approx_inv == True:
        left_term = K21 @ U
        middle_term = np.linalg.inv(Lam + U.T @ K21.T @ K21 @ U + 1e-5 * np.eye(m))
        K22_inv = left_term @ middle_term @ left_term.T
    else:
        K22_inv = np.linalg.inv(K22_approx)

    return K22_approx, K22_inv


def Nystrom_posterior(x_data, y_data, x_pred, kernel: callable, label_noise: float,
                      m: int, approx_inv: bool = True, **kwargs):
    # x are the test points with no associated y, A is the set of training
    # points for which y is observed
    K_AA, KAA_inv = Nystrom_approx(x_data, kernel, label_noise, m, approx_inv=approx_inv,
                                   **kwargs)
    K_xA = kernel(x_pred, x_data, label_noise=None, **kwargs)
    K_xx = kernel(x_pred, x_pred, label_noise=None, **kwargs)

    # Usually, mean functions are defined as mu(xi) = 0
    mu_x = 0
    mu_A = 0
    y = (y_data[:, 0] - mu_A)

    mu_ = mu_x +  K_xA @ KAA_inv @ y
    K_ = K_xx - K_xA @ KAA_inv @ K_xA.T

    return mu_, K_


def marginal_likelihood(theta, x_data, y_data, kernel):
    label_noise, lengthscale, coeff = theta
    # Eq. 4.38
    Kf_theta = kernel(x_data, x_data, None, lengthscale, coeff)
    Ky_theta = Kf_theta + np.eye(Kf_theta.shape[0]) * label_noise ** 2
    Kyth_inv = np.linalg.inv(Ky_theta)
    alpha = Kyth_inv @ y_data
    
    gradient_Kyth = 1 # ????
    gradient_logp = 0.5 * np.trace(alpha @ alpha.T - Kyth_inv * gradient_Kyth)
    # ????
    return


#%%
from datetime import datetime 
num_data = 2000
num_pred = 500
ndim_fourier_feats = 500
Nystrom_dim = 500

lengthscale = .5 # ??? When this is small, Nystrom is super bad. Why
coeff = .5  # ??? When this is large, Nystrom is super bad. Why?
label_noise = .5 # ??? When this is large, Nystrom works much better. Why?

# Generate data
x_data, y_data = generate_sinusoid_data(num_data)

# Make a array of x-data to predict: Evenly sample range of x_data
x_pred = np.linspace(x_data.min()*1.1, x_data.max()*1.1, num_pred).reshape(-1, 1)

## Exact posterior inference
t0 = datetime.now() 
exact_mu, exact_K = exact_gp_posterior(x_data, y_data, x_pred, kernel=gaussian_kernel,
                                       label_noise=label_noise,
                                       lengthscale=lengthscale, coeff=coeff)
t1 = datetime.now() - t0
print('Time elapsed (hh:mm:ss.ms) {}'.format(t1))

## Inference based on RFF using same kernel as for exact
t2 = datetime.now() 
rff_mu, rff_K, iS = rff_posterior(x_data, y_data, x_pred, lengthscale,
                              coeff, ndim_fourier_feats, label_noise) 
t3 = datetime.now() - t2
print('Time elapsed (hh:mm:ss.ms) {}'.format(t3))

## Inference based on Nystrom approximation using same kernel as for exact
t4 = datetime.now() 
Nys_mu, Nys_K = Nystrom_posterior(x_data, y_data, x_pred, gaussian_kernel,
                                  m=Nystrom_dim, label_noise=label_noise,
                                  approx_inv=False, lengthscale=lengthscale,
                                  coeff=coeff) 
t5 = datetime.now() - t2
print('Time elapsed (hh:mm:ss.ms) {}'.format(t5))


#%%
# Plot data points
fig, axs = plt.subplots(2, 3, figsize=(10, 8))
ax = axs[0, 0]
ax.set_ylim((-2.2, 3))
ax.scatter(x_data, y_data, color='k', s=1)

ax.plot(x_pred[:, 0], exact_mu[:], color='tab:orange')
# Diag extracts variance of data point x around prediction mu
ax.fill_between(x_pred[:, 0],
                exact_mu - 2 * np.diag(exact_K) ** 0.5,
                exact_mu + 2 * np.diag(exact_K) ** 0.5,
                color='tab:orange', alpha=0.2)
ax.set_title(f'full features ({num_data})', size=17)
ax.set_xlabel('$x$', fontsize=16)
ax.set_ylabel('$y$', fontsize=16)

ax = axs[1, 0]
ax.imshow(exact_K)
ax.set_xticks([])
ax.set_yticks([])
ax.set_xlabel('$x$', fontsize=16)
ax.set_ylabel('$x$', fontsize=16)

ax = axs[0, 1]
ax.scatter(x_data, y_data, color='k', s=1)
ax.plot(x_pred[:, 0], rff_mu[:], color='tab:orange')
# Diag extracts variance of data point x around prediction mu
ax.fill_between(x_pred[:, 0],
                rff_mu - 2 * rff_K ** 0.5,
                rff_mu + 2 * rff_K ** 0.5,
                color='tab:orange', alpha=0.2)
ax.set_yticks([])
ax.set_xlabel('$x$', fontsize=16)
ax.set_title(f'RFF ({ndim_fourier_feats})', size=17)
ax = axs[1, 1]
ax.imshow(iS)
ax.set_xticks([])
ax.set_yticks([])
ax.set_xlabel('$x$', fontsize=16)

ax = axs[0, 2]
ax.scatter(x_data, y_data, color='k', s=1)
ax.plot(x_pred[:, 0], Nys_mu[:], color='tab:orange')
# Diag extracts variance of data point x around prediction mu
ax.fill_between(x_pred[:, 0],
                Nys_mu - 2 * np.diag(Nys_K) ** 0.5,
                Nys_mu + 2 * np.diag(Nys_K) ** 0.5,
                color='tab:orange', alpha=0.2)
ax.set_title(f'Nyström ({Nystrom_dim})', size=17)
ax.set_xlabel('$x$', fontsize=16)
ax.set_yticks([])

ax = axs[1, 2]

ax.imshow(Nys_K)
ax.set_xticks([])
ax.set_yticks([])
ax.set_xlabel('$x$', fontsize=16)

fig.tight_layout()

#%%
# Sample from the posterior
# y_samples = sample_posterior(exact_mu, exact_K, n_samples=50)

# fig, ax = plt.subplots()
# ax.plot(x_pred, y_samples, color='grey', zorder=-1, lw=0.5)
# ax.scatter(x_data, y_data, color='tab:red', s=1, zorder=1)
# ax.set_xlabel('$x$', fontsize=16)
# ax.set_ylabel('$y$', fontsize=16)